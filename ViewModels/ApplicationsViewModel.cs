﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tennis.Models;

namespace Tennis.ViewModels
{
    public class ApplicationsViewModel
    {
        public List<TournamentPlayers> tournamentPlayers { get; set; }
        public User User { get; set; }
        public Guest Guest { get; set; }
        public PlayersPair Pair { get; set; }
    }
}
