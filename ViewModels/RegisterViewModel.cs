using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Tennis.Models;

namespace Tennis.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage ="Email")]
        [Remote(action: "CheckName", controller: "Account", ErrorMessage = "EmailContains")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Аватар")]
        public string Avatar { get; set; }
        [Required(ErrorMessage ="Name")]
        [Display(Name = "Имя")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Country")]
        [Display(Name = "Страна")]
        public string Country { get; set; }
        [Required(ErrorMessage ="Category")]
        [Display(Name = "Категория")]
        public CategoryPerson Category { get; set; }
        [DateMinimumAge(1, ErrorMessage = "{0} должен быть больше {1} лет")]
        [Display(Name = "Дата рождения")]
        public DateTime DateOfBirth { get; set; }
        [Required(ErrorMessage ="Phone")]
        [StringLength(12,MinimumLength =10, ErrorMessage = "RequiredLength")]
        [Display(Name = "Номер телефон")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage ="Gender")]
        [Display(Name = "Пол")]
        public Gender Gender { get; set; }
        [Required(ErrorMessage ="Password")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
        [Required(ErrorMessage ="PasswordConfirm")]
        [Compare("Password", ErrorMessage = "PasswordCompare")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердить пароль")]
        public string PasswordConfirm { get; set; }
    }

    public enum CategoryPerson
    {
        Futures,
        Masters,
        Promasters
    }
    public enum Gender
    {
        Male,
        Female
    }
    public enum StatusPerson
    {
        Active,
        NonActive
    }
}