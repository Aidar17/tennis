﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tennis.Models;

namespace Tennis.ViewModels
{
    public class UserPageViewModel
    {
        public User User { get; set; }
        public IEnumerable<Tournament> AllTournaments { get; set; }
        public IEnumerable<Tournament> WinnerTournaments { get; set; }
        public IEnumerable<UserTournamentScore> Scores { get; set; }
    }
}
