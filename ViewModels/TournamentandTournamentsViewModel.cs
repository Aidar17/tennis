﻿using Tennis.Models;
using System;
using System.Collections.Generic;

namespace Tennis.ViewModels
{
    public class TournamentandTournamentsViewModel
    {
        public Tournament Tournament { get; set; }
        public List<Tournament> Tournaments { get; set; }
    }
}
