﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tennis.Models;

namespace Tennis.ViewModels
{
    public class NewsAndListOfNewsViewModel
    {
        public News News { get; set; }
        public List<News> ListOfNews { get; set; }
    }
}
