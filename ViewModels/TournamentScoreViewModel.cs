﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tennis.Models;

namespace Tennis.ViewModels
{
    public class TournamentScoreViewModel
    {
        public Tournament tournament { get; set; }
        public List<UserTournamentScore> tournamentScores { get; set; }
    }

}
