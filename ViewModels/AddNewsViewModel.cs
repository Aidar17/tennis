﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tennis.ViewModels
{
    public class AddNewsViewModel
    {
        [Required(ErrorMessage = "Topic")]
        [Display(Name = "Тема")]
        public string Topic { get; set; }
        [Required(ErrorMessage = "Content")]
        [Display(Name = "Содержание ")]
        public string Content { get; set; }
       
        [Display(Name = "Фото")]
        public string Photo { get; set; }

    }
}
