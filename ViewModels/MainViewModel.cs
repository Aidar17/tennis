﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tennis.Models;

namespace Tennis.ViewModels
{
    public class MainViewModel
    {
        public List<User> Users { get; set; }
        public List<Tournament> Tournaments { get; set; }
        public List<Sponsor> Sponsors { get; set; }
        public IEnumerable<Sponsor> Sponsorss { get; set; }
        public UserPageViewModel PageViewModel { get; set; }
        public PageViewModel PageViewModelSponsor { get; set; }
        public List<News> LastNews { get; set; }
        public Models.Type Type { get; set; }
        public CategoryTournament Level { get; set; }
    }
}
