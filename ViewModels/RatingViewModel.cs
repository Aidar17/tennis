﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tennis.Models;

namespace Tennis.ViewModels
{
    public class RatingViewModel
    {
        public List<Rating> Ratings { get; set; }
        public List<User> Users { get; set; }
    }
}
