﻿using Tennis.Models;
using System;
using System.Collections.Generic;

namespace Tennis.ViewModels
{
    public class SponsorandSponsorsViewModel
    {
        public Sponsor Sponsor { get; set; }
        public List<Sponsor> Sponsors { get; set; }
    }
}
