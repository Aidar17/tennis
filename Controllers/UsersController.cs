using Microsoft.AspNetCore.Mvc;
﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tennis.Models;
using Tennis.ViewModels;

namespace Tennis.Controllers
{
    public class UsersController : Controller
    {
        public UsersContext _db;
        private readonly UserManager<User> _userManager;

        public UsersController(UsersContext db, UserManager<User> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        public IActionResult Rating()
        {
            var users = _db.ContextUsers.Where(x => x.Points > 0).OrderBy(x => x.Points).ToList();
            return View(users);
        }

        private async Task<User> GetCurrentUser()
        {
            return await _userManager.GetUserAsync(HttpContext.User);
        }

        public IActionResult UserPage()
        {
            User user = GetCurrentUser().Result;
            var tournaments = _db.Tournaments.ToList();
            var listUTS = _db.userTournaments.Where(x => x.UserId == user.Id).ToList();

            var allUserTournaments = new List<Tournament>();

            foreach (var item in listUTS)
            {
                if (item.Tournament != null)
                {
                    allUserTournaments.Add(item.Tournament);
                }
            }

            var winListUTS = listUTS.Where(x => x.Place <= 3);

            var winnerTournaments = new List<Tournament>();

            foreach(var item in winListUTS)
            {
                if(item.Tournament != null)
                {
                    winnerTournaments.Add(item.Tournament);
                }
            }

            var model = new UserPageViewModel()
            {
                User = user,
                AllTournaments = allUserTournaments,
                WinnerTournaments = winnerTournaments,
                Scores = winListUTS

            };

            return View(model);
        }


        [HttpPost]
        public OkObjectResult Edit(User user)
        {
            User EditedUser = _db.ContextUsers.FirstOrDefault(x => x.Id == user.Id);
            if (user != null)
            {
                if(user.Email != EditedUser.Email && user.Email != null)
                    EditedUser.Email = user.Email;
                if(user.PhoneNumber != EditedUser.PhoneNumber && user.PhoneNumber != null)
                    EditedUser.PhoneNumber = user.PhoneNumber;
                if(user.Country != EditedUser.Country && user.Country != null)
                    EditedUser.Country = user.Country;
                if(user.Category != EditedUser.Category)
                    EditedUser.Category = user.Category;
                if(user.Gender != EditedUser.Gender)
                    EditedUser.Gender = user.Gender;     
                if(user.DateOfBirth != EditedUser.DateOfBirth)
                    EditedUser.DateOfBirth = user.DateOfBirth;
                _db.ContextUsers.Update(EditedUser);
                _db.SaveChanges();
                return Ok(user);
            }
            else
            {
                return Ok("Не фартануло");
            }
        }
    }
}
