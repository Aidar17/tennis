using System;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Tennis.Models;
using Tennis.Services;
using Tennis.ViewModels;

namespace Tennis.Controllers
{
    public class AccountController : Controller
    {

        public UsersContext _db;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        IWebHostEnvironment _appEnvironment;
        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, IWebHostEnvironment appEnvironment, UsersContext db)
        {

            _userManager = userManager;
            _signInManager = signInManager;
            _appEnvironment = appEnvironment;
            _db = db;
        }
        public IActionResult SetLanguage(string returnUrl, string ru, string en)
        {
            string culture = string.Empty;
            if (ru == null)
                culture = en;
            else if (en == null)
                culture = ru;
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1)}
                );

            return LocalRedirect(returnUrl);
        }
        [AcceptVerbs("GET", "POST")]
        public bool CheckName(string email)
        {
            if (_db.ContextUsers.Any(b => b.Email == email ))
                return false;
            else
                return true;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model, IFormFile image)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    model.Avatar = "/avatars/" + image.FileName;
                    using (var stream = new FileStream(_appEnvironment.WebRootPath + model.Avatar, FileMode.Create))
                    {
                        await image.CopyToAsync(stream);
                    }
                }
                else
                    model.Avatar = "/avatars/no-avatar-pic.jpg";

                User user = new User
                {
                    Email = model.Email,
                    Name = model.Name,
                    Avatar = model.Avatar,
                    Gender = model.Gender,
                    PhoneNumber = model.PhoneNumber,
                    Country = model.Country,
                    Category = model.Category,
                    DateOfBirth=model.DateOfBirth,
                    Points=0,
                    Status =StatusPerson.Active,
                    UserName =model.Email,
                    ContributionTime=DateTime.Now
                };

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "user");
                    await _signInManager.SignInAsync(user, false);
                    SetRating(user);
                    return RedirectToAction("Index", "Home");
                }
                foreach (var error in result.Errors)
                    ModelState.AddModelError(string.Empty, error.Description);
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {

            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByEmailAsync(model.Email);
                try
                {
                    var result = await _signInManager.PasswordSignInAsync(
                   user,
                   model.Password,
                   model.RememberMe,
                   false
                   );
                    if (result.Succeeded)
                    {
                        if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                            return Redirect(model.ReturnUrl);
                        var userRole = await _userManager.GetRolesAsync(user);
                        if (userRole[0] == "admin")
                            return RedirectToAction("Index", "Admin");
                        else
                            return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                        return View(model);
                    }
                }
                catch(Exception)
                {
                    ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                    return View(model);
                }
            }
            else
                return View(model);
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<OkObjectResult> Forgot(string email)
        {
            if (email != null)
            {
                var user = await _userManager.FindByEmailAsync(email) ;
                if (user == null || (await _userManager.IsEmailConfirmedAsync(user)))
                    return Ok("Юзера с такой почтой не существует");
                else
                {
                    var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                    EmailService emailService = new EmailService();
                    await emailService.SendEmailAsync(user.Email, "Reset Password", $"Для сброса пароля пройдите по ссылке: <a href='{callbackUrl}'>link</a>");
                    return Ok("На ваш почтовый ящик было выслано письмо");
                }
            }
            return Ok("Введите данные корректно");
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            return code == null ? View("Error") : View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
                return View("ResetPasswordConfirmation");
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
                return View("ResetPasswordConfirmation");
            foreach (var error in result.Errors)
                ModelState.AddModelError(string.Empty, error.Description);
            return RedirectToAction("Login");
        }

        public async Task<IActionResult> ChangePassword(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            ChangePasswordViewModel model = new ChangePasswordViewModel { Id = user.Id, Email = user.Email };
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    var _passwordValidator =
                        HttpContext.RequestServices.GetService(typeof(IPasswordValidator<User>)) as IPasswordValidator<User>;
                    var _passwordHasher =
                        HttpContext.RequestServices.GetService(typeof(IPasswordHasher<User>)) as IPasswordHasher<User>;

                    IdentityResult result =
                        await _passwordValidator.ValidateAsync(_userManager, user, model.NewPassword);
                    if (result.Succeeded)
                    {
                        user.PasswordHash = _passwordHasher.HashPassword(user, model.NewPassword);
                        await _userManager.UpdateAsync(user);
                        return RedirectToAction("UserPage", "Users");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Пользователь не найден");
                }
            }
            return View(model);
        }

        public void SetRating(User user)
        {
            if (user.Gender == Gender.Male)
            {
                for (int i = 0; i < 3; i++)
                {
                    var rating = new Rating()
                    {
                        UserId = user.Id,
                        Level = (CategoryPerson)i,
                        Points = 0,
                        Type = Models.Type.ManOne
                    };
                    _db.Ratings.Add(rating);

                }
                for (int i = 0; i < 3; i++)
                {
                    var rating = new Rating()
                    {
                        UserId = user.Id,
                        Level = (CategoryPerson)i,
                        Points = 0,
                        Type = Models.Type.ManTwo
                    };
                    _db.Ratings.Add(rating);

                }
                for (int i = 0; i < 3; i++)
                {
                    var rating = new Rating()
                    {
                        UserId = user.Id,
                        Level = (CategoryPerson)i,
                        Points = 0,
                        Type = Models.Type.Mixed
                    };
                     _db.Ratings.Add(rating);
                    
                }
                 _db.SaveChanges();
            }
            else
            {
                for (int i = 0; i < 3; i++)
                {
                    var rating = new Rating()
                    {
                        UserId = user.Id,
                        Level = (CategoryPerson)i,
                        Points = 0,
                        Type = Models.Type.WomenOne
                    };
                    _db.Ratings.Add(rating);
                }
                for (int i = 0; i < 3; i++)
                {
                    var rating = new Rating()
                    {
                        UserId = user.Id,
                        Level = (CategoryPerson)i,
                        Points = 0,
                        Type = Models.Type.WomenTwo
                    };
                    _db.Ratings.Add(rating);
                }
                for (int i = 0; i < 3; i++)
                {
                    var rating = new Rating()
                    {
                        UserId = user.Id,
                        Level = (CategoryPerson)i,
                        Points = 0,
                        Type = Models.Type.Mixed
                    };
                    _db.Ratings.Add(rating);

                }
                _db.SaveChanges();
            }

        }
    }
}
