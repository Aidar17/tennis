﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Tennis.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tennis.Models;
using Microsoft.AspNetCore.Http;

namespace Tennis.Controllers
{
    public class TournumentScoreController : Controller
    {
        private readonly UsersContext _db;
        private readonly UserManager<User> _userManager;

        public TournumentScoreController(UsersContext db, UserManager<User> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        private async Task<User> GetCurrentUser()
        {
            return await _userManager.GetUserAsync(HttpContext.User);
        }

        [HttpGet]
        [Authorize(Roles = "user")]
        public async Task<List<TournamentPlayers>> GetTournamentUsers()
        {
            return await _db.AllTournamentPlayers.Where(u => u.UserId == GetCurrentUser().Result.Id).ToListAsync();
        }

        [Authorize(Roles = "user")]
        public async Task<IActionResult> Apply(string tournament_Id, string pref)
        {
            if (User.Identity.IsAuthenticated)
            {
                UserTournamentScore score = new UserTournamentScore();
                Tournament tournament = await _db.Tournaments.FirstOrDefaultAsync(t => t.Id == tournament_Id);
                if (tournament != null)
                {
                    User user = GetCurrentUser().Result;
                    if (Check((int)user.Category, (int)tournament.Category))
                    {
                        if (tournament.Type == Type.ManOne)
                        {
                            if (user.Gender == Gender.Female)
                                return Ok("В этом турнире могут участвовать только мужчины");
                        } else if (tournament.Type == Type.WomenOne)
                        {
                            if (user.Gender == Gender.Male)
                                return Ok("В этом турнире могут участвовать только женщины");
                        }

                        List<TournamentPlayers> numberOfScore = await _db.AllTournamentPlayers
                            .Where(u => u.TournamentId == tournament.Id && u.Status == StatusPlayers.Wait)
                            .ToListAsync();

                        TournamentPlayers userTournament = new TournamentPlayers()
                        {
                            Status = StatusPlayers.Wait,
                            UserId = user.Id,
                            TournamentId = tournament_Id,
                            Preferences = pref
                        };

                        if (numberOfScore.Count() < tournament.UserCount)
                        {
                            _db.AllTournamentPlayers.Add(userTournament);
                            await _db.SaveChangesAsync();
                            return Ok("ОК");
                        }
                        else
                        {
                            userTournament.Status = StatusPlayers.Wait_In_Spare;
                            _db.AllTournamentPlayers.Add(userTournament);
                            await _db.SaveChangesAsync();
                            return Ok("К сожелению необходимое количество участников уже набралось. Вы добавлены в качестве запасного игрока");
                        }
                    }
                    else
                        return Ok("К сожелению вы не можете оставить заявку на участие в этом турнире, так как ваша категория не соответствует категории турнира");
                }
                else
                    return Ok("Турнир не найден");
            }
            else
                return Ok("Для того чтобы принять участия, вам надо зарегистрироваться на сайте");
        }

        [HttpPost]
        public async Task<IActionResult> ApplyGuest(Guest guest, string id)
        {
            if (_db.Tournaments.Any(t => t.Id == id))
            {
                List<TournamentPlayers> numberOfScore = await _db.AllTournamentPlayers.Where(u => u.TournamentId == id).ToListAsync();
                Guest guest1 = _db.Guests.FirstOrDefault(g => g.PhoneNumber == guest.PhoneNumber);
                if (guest1 == null)
                {
                    _db.Guests.Add(guest);
                    _db.SaveChanges();
                }
                else
                    guest = guest1;
                if(_db.userTournaments.Any(t => t.GuestId == guest.Id && t.TournamentId == id))
                    return Ok("Вы уже учатник этого турнира!");
                if (_db.AllTournamentPlayers.Any(t => t.GuestId == guest.Id && t.TournamentId == id))
                    return Ok("Вы уже подали заявку на участие в этом турнире! Ожидайте пока администратор одобрит вашу заявку");
                TournamentPlayers userTournament = new TournamentPlayers
                {
                    Status = StatusPlayers.Wait,
                    GuestId = guest.Id,
                    TournamentId = id
                };
                Tournament tournament = await _db.Tournaments.FirstOrDefaultAsync(t => t.Id == id);
                if (numberOfScore.Count() < tournament.UserCount)
                {
                    _db.AllTournamentPlayers.Add(userTournament);
                    await _db.SaveChangesAsync();
                    return Ok("ОК");
                }
                else
                {
                    userTournament.Status = StatusPlayers.Wait_In_Spare;
                    _db.AllTournamentPlayers.Add(userTournament);
                    await _db.SaveChangesAsync();
                    return Ok("К сожелению необходимое количество участников уже набралось. Вы добавлены в качестве зопасного игрока");
                }
            }
            else
                return Ok("Ой. Турнир не найден");
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> AddingGuestByAdmin(Guest guest, string id)
        {
            if (_db.Tournaments.Any(t => t.Id == id))
            {
                List<UserTournamentScore> numberOfScore = await _db.userTournaments.Where(u => u.TournamentId == id).ToListAsync();
                Guest guest1 = _db.Guests.FirstOrDefault(g => g.PhoneNumber == guest.PhoneNumber);
                if (guest1 == null)
                {
                    _db.Guests.Add(guest);
                    _db.SaveChanges();
                }
                else
                    guest = guest1;
                if (_db.userTournaments.Any(t => t.GuestId == guest.Id && t.TournamentId == id))
                    return Ok("Игрок с таким номером телефона, уже добавлен!");
                UserTournamentScore userTournament = new UserTournamentScore
                {
                    Status = StatusPlayers.In_Tour,
                    GuestId = guest.Id,
                    TournamentId = id
                };
                Tournament tournament = await _db.Tournaments.FirstOrDefaultAsync(t => t.Id == id);
                if (numberOfScore.Count() < tournament.UserCount)
                {
                    _db.userTournaments.Add(userTournament);
                    await _db.SaveChangesAsync();
                    return Ok(tournament);
                }
                else
                {
                    userTournament.Status = StatusPlayers.Wait_In_Spare;
                    _db.userTournaments.Add(userTournament);
                    await _db.SaveChangesAsync();
                    return Ok("Необходимое количество участников уже набралось. Гость добавлен в качестве зопасного игрока");
                }
            }
            else
                return Ok("Ой. Турнир не найден");
        }

        private bool Check(int CategoryUser, int CategoryTournament)
        {
            if (CategoryUser == 2)
            {
                if (CategoryUser == CategoryTournament)
                    return true;
                else
                    return false;
            }
            else
            {
                if (CategoryUser == CategoryTournament || CategoryTournament == CategoryUser + 1)
                    return true;
                else
                    return false;
            }
        }
    }
}
