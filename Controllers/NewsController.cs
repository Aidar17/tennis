﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Tennis.Models;
using Tennis.ViewModels;

namespace Tennis.Controllers
{
    public class NewsController : Controller
    {
        private UsersContext _db;

        IWebHostEnvironment _appEnvironment;

        public NewsController(UsersContext db, IWebHostEnvironment appEnvironment)
        {
            _db = db;
            _appEnvironment = appEnvironment;
        }

        public IActionResult Index()
        {
            var news = _db.News.OrderByDescending(n => n.DateOfPublication).ToList();
            NewsAndListOfNewsViewModel model = new NewsAndListOfNewsViewModel()
            {
               
                ListOfNews = news
            };
            return View(model);
        }

        
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Add(NewsAndListOfNewsViewModel model, IFormFile image)
        {

            News news = model.News;
            news.DateOfPublication = DateTime.Now;
            if (image != null)
            {
                news.Photo = "/news/NewsPhotos/" + image.FileName;
                using (var stream = new FileStream(_appEnvironment.WebRootPath + news.Photo, FileMode.Create))
                {
                    await image.CopyToAsync(stream);
                }
            }
            else
            {
                news.Photo = "/news/NewsPhotos/no-avatar-pic.jpg";
            }
            await _db.News.AddAsync(news);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index", "News");
        }
        

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<JsonResult> Delete(string idNews)
        {
            News news = _db.News.FirstOrDefault(r => r.Id == idNews);
            if (news != null)
            {
                _db.News.Remove(news);
                await _db.SaveChangesAsync();
                return Json("Success");
            }
            else return Json("DeleteError");
        }

        [Authorize(Roles = "admin")]
        public IActionResult Edit(string id)
        {
            if (id != null)
            {
                News news = _db.News.FirstOrDefault(n => n.Id == id);
                if (news != null)
                    return View(news);
            }
            return NotFound();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Edit(News news, IFormFile image)
        {
            if (image != null)
            {
                news.Photo = "/news/NewsPhotos/" + image.FileName;
                using (var stream = new FileStream(_appEnvironment.WebRootPath + news.Photo, FileMode.Create))
                {
                    await image.CopyToAsync(stream);
                }
            }
            else
            {
                news.Photo = "/news/NewsPhotos/no-avatar-pic.jpg";
            }
            _db.News.Update(news);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
