using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Tennis.Models;
using Tennis.ViewModels;

namespace Tennis.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UsersContext _db;
        public HomeController(ILogger<HomeController> logger, UsersContext db)
        {
            _logger = logger;
            _db = db;
        }

        public async Task<IActionResult> Index(int page = 1)
        {
            int pagesize = 6;
            var users = _db.ContextUsers.Where(x => x.Points > 0).OrderBy(x => x.Points).ToList();
            var tournaments = _db.Tournaments.OrderBy(x => x.Date).Where(x => x.Status == StatusTournament.Will).ToList();
            var _users = _db.ContextUsers.Where(x => x.Points > 0).OrderBy(x => x.Points).ToList();
            var tournamets = _db.Tournaments.OrderByDescending(x => x.Date).ToList();
            int numberOfrecords = 5;
            var lastFiveNews = _db.News.OrderByDescending(x => x.DateOfPublication).Take(numberOfrecords).ToList();
            IQueryable<Sponsor> sponsors = _db.Sponsors.OrderBy(x => x.DateCreate);
            var count = await sponsors.CountAsync();
            var items = await sponsors.Skip((page - 1) * pagesize).Take(pagesize).ToListAsync();
            PageViewModel pageViewModel = new PageViewModel(count, page, pagesize);
            MainViewModel ivm = new MainViewModel()
            {
                Users = _users,
                Tournaments = tournaments,
                Sponsorss = sponsors,
                PageViewModelSponsor = pageViewModel,
                Sponsors = items,
                LastNews = lastFiveNews
            };
            return View(ivm);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpPost]
        public IActionResult GetTopRating(Models.Type type, CategoryPerson level)
        {
            var ratingViewModel = new RatingViewModel()
            {
                Ratings = _db.Ratings.Where(r => r.Type == type && r.Level == level).OrderByDescending(r => r.Points).Take(10).ToList()
            };
            var usersList = new List<User>();
            foreach (var item in ratingViewModel.Ratings)
            {
                var user = _db.Users.FirstOrDefault(u => u.Id == item.UserId);
                usersList.Add(user);
            };
            ratingViewModel.Users = usersList;
            return Json(new { ratingViewModel });
        }
    }
}
