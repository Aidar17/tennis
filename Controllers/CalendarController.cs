﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tennis.Models;

namespace Tennis.Controllers
{
    public class CalendarController : Controller
    {
        private UsersContext _db;
        public CalendarController(UsersContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<OkObjectResult> GetEvents()
        {

            var events = await _db.Tournaments.ToListAsync();
            return Ok(events);
        }

    }
   
}
