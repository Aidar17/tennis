﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tennis.Models;
using Tennis.ViewModels;
namespace Tennis.Controllers
{
    public class TournamentController : Controller
    {
        private UsersContext _db;
        public TournamentController(UsersContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var tournamets = _db.Tournaments.OrderByDescending(x => x.Date).ToList();
            return View(tournamets);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<OkObjectResult> Create(Tournament tournament)
        {
            if (tournament != null)
            {
                if (tournament.Date > DateTime.Now)
                {
                    tournament.Status = StatusTournament.Will;
                }
                else
                {
                    tournament.Status = StatusTournament.Was;
                }
                await _db.Tournaments.AddAsync(tournament);
                await _db.SaveChangesAsync();
                return Ok(tournament);
            }
            return Ok("There are some troubles");
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public OkObjectResult ConfirmationOfClosing(List<string> playersId, List<int> postitions, string Id)
        {
            if (playersId != null && Id != null && postitions != null)
            {
                var _tournament = _db.Tournaments.FirstOrDefault(x => x.Id == Id);
                var tournamentScores = _db.userTournaments.Where(x => x.TournamentId == Id).OrderByDescending(x => x.Points).ToList();
                
                foreach (var item in tournamentScores)
                {
                    for (int i = 0; i < playersId.Count; i++)
                    {
                        if (item.UserId == playersId[i]) 
                        {
                            item.Place = postitions[i];
                            item.Tournament.Status = StatusTournament.Was;
                            _db.userTournaments.Update(item);
                            _db.SaveChanges();
                            
                        }
                        if (item.GuestId == playersId[i])
                        {
                            item.Place = postitions[i];
                            item.Tournament.Status = StatusTournament.Was;
                            _db.userTournaments.Update(item);
                            _db.SaveChanges();

                        }
                    }
                   
                }
                if (_tournament.Status != StatusTournament.Was)
                {
                    return Ok("Не получилось, попробуйте еще раз");

                }
                return Ok("ОК");
            }
            else
                return Ok("Не получилось, попробуйте еще раз");
        }

        [Authorize(Roles = "admin")]
        public IActionResult Close(string Id)
        {
            var tournament = _db.Tournaments.FirstOrDefault(x => x.Id == Id);
            if (tournament != null && tournament.Status != StatusTournament.Was)
            {
               var tournamentScores = _db.userTournaments.Where(x => x.TournamentId == Id).OrderByDescending(x => x.Points).ToList();
                foreach(var i in tournamentScores)
                {
                    if(i.UserId != null)
                        i.User = _db.Users.FirstOrDefault(u => u.Id == i.UserId);
                    else if(i.GuestId != null)
                        i.Guest = _db.Guests.FirstOrDefault(u => u.Id == i.GuestId);
                }
                TournamentScoreViewModel scoreViewModel = new TournamentScoreViewModel()
                {
                    tournament = tournament,
                    tournamentScores = tournamentScores
                };
               return View(scoreViewModel);
            }
            return View();
        }
        [Authorize(Roles = "admin")]
        public ActionResult CloseTournament(string id)
        {
            var tournament = _db.Tournaments.FirstOrDefault(t => t.Id == id);
            return View(tournament);
        }
        [Authorize(Roles = "admin")]
        [HttpPost, ActionName("CloseTournament")]
        public ActionResult CloseConfirmed(string id)
        {
            var tournament = _db.Tournaments.FirstOrDefault(t => t.Id == id);
            tournament.Status = StatusTournament.Was;
            _db.Tournaments.Update(tournament);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult DetailsTournament(string Id)
        {
            var tournament = _db.Tournaments.FirstOrDefault(t => t.Id == Id);
           
            if (tournament != null)
            {
                var userTournament = _db.userTournaments.Where(u => u.TournamentId == Id && u.Status == StatusPlayers.In_Tour).ToList();
                ViewBag.TournamentRemainingSeats = tournament.UserCount - userTournament.Count;
                return View(tournament);
            }
            return NotFound("Турнир не найден!");
        }


        [HttpPost]
        [Authorize(Roles = "admin")]

        public IActionResult TournamentRemove(string Id)
        {
            var tournament = _db.Tournaments.FirstOrDefault(t => t.Id == Id);
            if(tournament != null)
            {
                var tournamentPlayers = _db.AllTournamentPlayers.Where(p => p.TournamentId == Id).ToList();
                var userTournament = _db.userTournaments.Where(u => u.TournamentId == Id).ToList();
                var pairPlayers = _db.PlayersPairs.Where(p => p.TournamentId == Id).ToList();
                if (tournamentPlayers.Count != 0)
                    tournamentPlayers.ForEach(t => _db.Remove(t));
                if (userTournament.Count != 0)
                    userTournament.ForEach(t => _db.Remove(t));
                if (pairPlayers.Count != 0)
                    pairPlayers.ForEach(t => _db.Remove(t));
                _db.Remove(tournament);
                _db.SaveChanges();
                return Ok();
            }
            return NotFound("Турнир не найден!");
        }
    }
}
