﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tennis.Models;
using Tennis.ViewModels;
namespace Tennis.Controllers
{
    public class SponsorController : Controller
    {
        private UsersContext _db;
        IWebHostEnvironment _appEnvironment;
        public SponsorController(UsersContext db, IWebHostEnvironment appEnvironment)
        {
            _db = db;
            _appEnvironment = appEnvironment;
        }

        public IActionResult Index()
        {
            var sponsors = _db.Sponsors.OrderBy(x => x.DateCreate).ToList();
            SponsorandSponsorsViewModel ivm = new SponsorandSponsorsViewModel ()
            {

                Sponsors = sponsors
            };
            return View(ivm);
        }

        [HttpPost]
        public async Task<IActionResult> Create(SponsorandSponsorsViewModel viewModel, IFormFile image)
        {

            Sponsor sponsor = viewModel.Sponsor;
            sponsor.DateCreate = DateTime.Now;
            if (image != null)
            {
                sponsor.Avatar = "/avatars/SponsorAvatars/" + image.FileName;
                sponsor.Link = "https://" + viewModel.Sponsor.Link;
                using (var stream = new FileStream(_appEnvironment.WebRootPath + sponsor.Avatar, FileMode.Create))
                {
                    await image.CopyToAsync(stream);
                }
            }
            else
            {
                sponsor.Avatar = "/avatars/SponsorAvatars/no-avatar-pic.jpg";
            }
            await _db.Sponsors.AddAsync(sponsor);
            await _db.SaveChangesAsync();
            return Redirect("Index");
        }

        [HttpPost]
        public async Task<JsonResult> Delete(string sponsorId)
        {
            Sponsor sponsor = _db.Sponsors.FirstOrDefault(r => r.Id == sponsorId);
            if (sponsor != null)
            {
                _db.Sponsors.Remove(sponsor);
                await _db.SaveChangesAsync();
                return Json("Success");
            }
            else return Json("DeleteError");
        }

        public IActionResult MainSponsor(string id)
        {
            Sponsor sponsor = _db.Sponsors.FirstOrDefault(r=>r.Id==id);
            return View(sponsor);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Sponsor sponsor, IFormFile image)
        {

            var sponsornew = await _db.Sponsors.FirstOrDefaultAsync(u => u.Id == sponsor.Id);
            if (sponsornew != null)
            {
                sponsornew.Name = sponsor.Name == null ? sponsornew.Name : sponsor.Name;
                sponsornew.Link = sponsor.Link == null ? sponsornew.Link : sponsor.Link;
                sponsornew.Description = sponsor.Description == null ? sponsornew.Description : sponsor.Description;
                if (image != null)
                {
                    sponsor.Avatar = "/avatars/SponsorAvatars/" + image.FileName;
                    using (var stream = new FileStream(_appEnvironment.WebRootPath + sponsor.Avatar, FileMode.Create))
                    {
                        await image.CopyToAsync(stream);
                    }
                }
                else
                    sponsor.Avatar = sponsornew.Avatar;
                sponsornew.Avatar = sponsor.Avatar == null ? sponsornew.Avatar : sponsor.Avatar;
            }
            _db.Sponsors.Update(sponsornew);
            await _db.SaveChangesAsync();
            return Redirect("~/Sponsor/MainSponsor/" + sponsor.Id);
        }


    }
}
