﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Tennis.Models;

namespace Tennis.Controllers
{
    public class AdminController : Controller
    {
        public UsersContext _db;



        public AdminController(UsersContext db)
        {
            _db = db;
        }



        public IActionResult Index()
        {
            List<User> allUsers = _db.ContextUsers.Where(u => u.Email != "admin@admin.com").ToList();

            allUsers = (from i in allUsers
                        orderby i.Points descending
                        select i).ToList();

            return View(allUsers);
        }

        [Authorize(Roles = "admin")]
        public IActionResult Detail(string Id)
        {
            var user = _db.ContextUsers.FirstOrDefault(x => x.Id == Id);
            if (user != null)
            {
                return View(user);
            }
            else
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public OkObjectResult Switch(string Id)
        {
            var user = _db.ContextUsers.FirstOrDefault(x => x.Id == Id);
            if (user != null)
            {
                if (user.Status == ViewModels.StatusPerson.Active)
                {
                    user.Status = ViewModels.StatusPerson.NonActive;
                    _db.ContextUsers.Update(user);
                    _db.SaveChanges();
                }
                else if (user.Status == ViewModels.StatusPerson.NonActive)
                {
                    user.Status = ViewModels.StatusPerson.Active;
                    _db.ContextUsers.Update(user);
                    _db.SaveChanges();
                }
                return Ok("Статус был изменён");
            }
            return Ok("Произогла ошибка");
        }


        [Authorize(Roles = "admin")]
        [HttpPost]
        public OkObjectResult Edit(User u)
        {
            var user = _db.ContextUsers.FirstOrDefault(x => x.Id == u.Id);
            if (user != null)
            {
                if (user.Email != u.Email && u.Email != null)
                    user.Email = u.Email;
                if (user.Country != u.Country && u.Country != null)
                    user.Country = u.Country;
                if (user.DateOfBirth != u.DateOfBirth && u.DateOfBirth != null && u.DateOfBirth != DateTime.MinValue)
                    user.DateOfBirth = u.DateOfBirth;
                if (user.ContributionTime != u.ContributionTime && u.ContributionTime != null && u.ContributionTime != DateTime.MinValue)
                    user.ContributionTime = u.ContributionTime;
                if (user.Category != u.Category && u.Country != null)
                    user.Category = u.Category;
                if (user.Name != u.Name && u.Name != null)
                    user.Name = u.Name;
                if (user.Points != u.Points)
                    user.Points = u.Points;
                if (user.PhoneNumber != u.PhoneNumber && u.PhoneNumber != null)
                    user.PhoneNumber = u.PhoneNumber;
                _db.ContextUsers.Update(user);
                _db.SaveChanges();
                return Ok(user);

            }
            else
            {
                return Ok("");
            }
        }

        public IActionResult ExcelDownload()
        {
            List<User> allUsers = _db.ContextUsers.Where(u => u.Email != "admin@admin.com").ToList();

            allUsers = (from i in allUsers
                        orderby i.Points descending
                        select i).ToList();

            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Users");
                var currentRow = 1;
                int id = 1;
                worksheet.Cell(currentRow, 1).Value = "ID";
                worksheet.Cell(currentRow, 2).Value = "АДРЕС ЭЛЕКТРОННОЙ ПОЧТЫ";
                worksheet.Cell(currentRow, 3).Value = "ИМЯ";
                worksheet.Cell(currentRow, 4).Value = "МОБИЛЬНЫЙ  ТЕЛЕФОН";
                worksheet.Cell(currentRow, 5).Value = "АКТИВНЫЙ";

                foreach (var user in allUsers)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = id;
                    worksheet.Cell(currentRow, 2).Value = user.Email;
                    worksheet.Cell(currentRow, 3).Value = user.Name;
                    worksheet.Cell(currentRow, 4).Value = user.PhoneNumber;
                    worksheet.Cell(currentRow, 5).Value = user.Status;
                    id++;

                }
                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "Users.xlsx"
                        );
                }
            }
        }

        /* Этот метод был создан для создания PDF документа и требует доработки так как не отображается кириллица.
         * 
         * public IActionResult PdfDownload()
         {
             List<User> allUsers = _db.ContextUsers.Where(u => u.Email != "admin@admin.com").ToList();

             allUsers = (from i in allUsers
                         orderby i.Points descending
                         select i).ToList();

             UserReport report = new UserReport();
             byte[] bytes = report.PrepareReport(allUsers);
             return File(bytes, "application/pdf");
         }*/
    }
}
