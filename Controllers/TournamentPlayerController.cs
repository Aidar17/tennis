﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Tennis.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tennis.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Tennis.Controllers
{
    public class TournamentPlayerController : Controller
    {
        private readonly UsersContext _db;
        private readonly UserManager<User> _userManager;
        public TournamentPlayerController(UsersContext db, UserManager<User> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        [Authorize(Roles = "admin")]
        public IActionResult Application(string id)
        {
            var tournaments = _db.Tournaments.ToList();
            var users = _db.ContextUsers.ToList();
            var guests = _db.Guests.ToList();
            var applications = _db.AllTournamentPlayers.Where(a => a.Status == StatusPlayers.Wait && a.TournamentId == id).ToList();

            var playersList = new List<string>();
            for (int i = 0; i < applications.Count; i++)
            {
                if (applications[i].Guest != null && applications[i].Status == StatusPlayers.Wait)
                    playersList.Add(applications[i].Guest.Name);
                else if (applications[i].User != null && applications[i].Status == StatusPlayers.Wait)
                    playersList.Add(applications[i].User.Name);
            }

            SelectList players = new SelectList(playersList);
            ViewBag.IdTournament = id;
            ViewBag.Players = players;
            return View(applications);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<List<TournamentPlayers>> CreateDropDown(string id, string type)
        {
            var tournaments = await _db.Tournaments.ToListAsync();
            var users = await _db.ContextUsers.ToListAsync();
            var guests = await _db.Guests.ToListAsync();
            List<TournamentPlayers> applications;
            if (type == null)
            {
                applications = await _db.AllTournamentPlayers.Where(a => a.Status == StatusPlayers.Wait && a.TournamentId == id).ToListAsync();
            }
            else
            {
                applications = await _db.AllTournamentPlayers.Where(a => a.Status == StatusPlayers.Not_approve && a.TournamentId == id).ToListAsync();
            }
            return applications;
        }

        [Authorize(Roles = "admin")]
        public async Task<IActionResult> ApproveApplication(string id)
        {
            var tournaments = await _db.Tournaments.ToListAsync();
            var users = await _db.ContextUsers.ToListAsync();
            var guests = await _db.Guests.ToListAsync();
            var applications = await _db.AllTournamentPlayers.Where(a => a.Status == StatusPlayers.In_Tour && a.TournamentId == id).ToListAsync();
            ViewBag.IdTournament = id;
            return View(applications);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult ApproveSingle(string id)
        {
            TournamentPlayers application = _db.AllTournamentPlayers.FirstOrDefault(a => a.Id == id);
            UserTournamentScore player = new UserTournamentScore
            {
                TournamentId = application.TournamentId,
                TournamentPlayerId = application.Id,
                Status = StatusPlayers.In_Tour
            };
            if (application.GuestId == null)
                player.UserId = application.UserId;
            else if (application.UserId == null)
                player.GuestId = application.GuestId;
            application.Status = StatusPlayers.In_Tour;
            _db.userTournaments.Add(player);
            _db.SaveChanges();
            return Redirect("~/TournamentPlayer/Application/" + application.TournamentId);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult ApprovePair(string Id, string playerTwo)
        {
            var tournaments = _db.Tournaments.ToList();
            var users = _db.ContextUsers.ToList();
            var guests = _db.Guests.ToList();
            var firstPlayer = _db.AllTournamentPlayers.FirstOrDefault(a => a.Id == Id);

            var members = _db.userTournaments.Where(m => m.TournamentId == firstPlayer.TournamentId && m.Status == StatusPlayers.In_Tour).ToList();
            if (members.Count > firstPlayer.Tournament.UserCount)
            {
                return Ok("OK");
            }

            var secondPlayer = _db.AllTournamentPlayers
                .Where(x => x.TournamentId == firstPlayer.TournamentId)
                .FirstOrDefault(x => x.Guest.Id == playerTwo || x.User.Id == playerTwo);

            var pair = new PlayersPair
            {
                Position = 1,
                TournamentPlayersFirstId = firstPlayer.Id,
                TournamentPlayersSecondId = secondPlayer.Id,
                TournamentId = firstPlayer.TournamentId
            };

            UserTournamentScore firstPlayerScore = new UserTournamentScore
            {
                TournamentId = firstPlayer.TournamentId,
                TournamentPlayerId = firstPlayer.Id,
                Status = StatusPlayers.In_Tour
            };

            UserTournamentScore secondPlayerScore = new UserTournamentScore
            {
                TournamentId = secondPlayer.TournamentId,
                TournamentPlayerId = secondPlayer.Id,
                Status = StatusPlayers.In_Tour
            };

            firstPlayer.Status = StatusPlayers.In_Tour;
            secondPlayer.Status = StatusPlayers.In_Tour;

            _db.PlayersPairs.Add(pair);
            _db.userTournaments.Add(firstPlayerScore);
            _db.userTournaments.Add(secondPlayerScore);
            _db.SaveChanges();

            var model = _db.AllTournamentPlayers
                .Where(a => a.Status == StatusPlayers.Wait && a.TournamentId == firstPlayerScore.TournamentId)
                .ToList();

            return RedirectToAction("Application", new { model });
        }

        [HttpGet]
        public IActionResult GetPairPlayers(string Id)
        {
            _db.Tournaments.ToList();
            _db.AllTournamentPlayers.ToList();
            _db.Guests.ToList();
            _db.Users.ToList();
            List<PlayersPair> players = _db.PlayersPairs.Where(p => p.TournamentId == Id).ToList();
            return Json(players);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult NotApprove(string id)
        {
            TournamentPlayers application = _db.AllTournamentPlayers.FirstOrDefault(a => a.Id == id);
            UserTournamentScore userTournament = _db.userTournaments.FirstOrDefault(a => a.TournamentPlayerId == id);
            if (application != null)
            {
                application.Status = StatusPlayers.Not_approve;
                if (userTournament != null)
                {
                    userTournament.Status = StatusPlayers.Not_approve;
                    _db.userTournaments.Update(userTournament);
                    _db.SaveChanges();
                }
                _db.AllTournamentPlayers.Update(application);
                _db.SaveChanges();
                return Redirect("~/TournamentPlayer/Application/" + application.TournamentId);
            }
            return NotFound("Заявка не найдена");
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult NotApprovePairPlay(string Id)
        {
            _db.Tournaments.ToList();
            _db.AllTournamentPlayers.ToList();
            _db.userTournaments.ToList();
            var pairPlayer = _db.PlayersPairs.FirstOrDefault(p => p.Id == Id);
            if(pairPlayer != null)
            {
                var userTournamentFirst = _db.userTournaments.FirstOrDefault(u => u.TournamentPlayerId == pairPlayer.TournamentPlayersFirst.Id);
                var userTournamentSecond = _db.userTournaments.FirstOrDefault(u => u.TournamentPlayerId == pairPlayer.TournamentPlayersSecond.Id);
                pairPlayer.TournamentPlayersFirst.Status = StatusPlayers.Not_approve;
                pairPlayer.TournamentPlayersSecond.Status = StatusPlayers.Not_approve;
                userTournamentFirst.Status = StatusPlayers.Not_approve;
                userTournamentSecond.Status = StatusPlayers.Not_approve;
                _db.userTournaments.Update(userTournamentFirst);
                _db.userTournaments.Update(userTournamentFirst);
                _db.AllTournamentPlayers.Update(pairPlayer.TournamentPlayersFirst);
                _db.AllTournamentPlayers.Update(pairPlayer.TournamentPlayersSecond);
                _db.Remove(pairPlayer);
                _db.SaveChanges();
                return Ok();
            }
            return NotFound("Заявка не найдена");
        }

        [Authorize(Roles = "admin")]
        public IActionResult DeclinedApplication(string id)
        {
            var tournaments = _db.Tournaments.ToList();
            var users = _db.ContextUsers.ToList();
            var guests = _db.Guests.ToList();
            ViewBag.IdTournament = id;
            var applications = _db.AllTournamentPlayers.Where(a => a.Status == StatusPlayers.Not_approve && a.TournamentId == id).ToList();
            return View(applications);
        }
        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult AcceptUser(string id)
            {
            TournamentPlayers application = _db.AllTournamentPlayers.FirstOrDefault(a => a.Id == id);
            var tournament = _db.Tournaments.FirstOrDefault(t => t.Id == application.TournamentId);
            var members = _db.userTournaments.Where(m => m.TournamentId == application.TournamentId && m.Status == StatusPlayers.In_Tour).ToList();
            if (members.Count > tournament.UserCount)
            {
                return Json(Ok("OK"));
            }
            else
            {
                UserTournamentScore player = _db.userTournaments.FirstOrDefault(a => a.TournamentPlayerId == application.Id);
                if(player == null)
                {
                    player = new UserTournamentScore();
                    player.TournamentId = application.TournamentId;
                    player.TournamentPlayerId = application.Id;
                    player.Status = StatusPlayers.In_Tour;
                    player.UserId = application.UserId;
                    player.GuestId = application.GuestId;
                    _db.userTournaments.Add(player);
                }
                else
                {
                    player.Status = StatusPlayers.In_Tour;
                    _db.userTournaments.Update(player);
                    _db.SaveChanges();
                }
                application.Status = StatusPlayers.In_Tour;
                _db.AllTournamentPlayers.Update(application);
                _db.SaveChanges();
                var users = _db.ContextUsers.ToList();
                var tournaments = _db.Tournaments.ToList();
                var allDeclinedPlayers = _db.AllTournamentPlayers.Where(a => a.Status == StatusPlayers.Not_approve && a.Id == id).ToList();
                return Json(allDeclinedPlayers);
            }
        }

    }
}
