using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tennis.Models
{
    public class UserTournamentScore
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public TournamentQueueStatus Queue { get; set; }
        public int Points { get; set; }
        public int Place { get; set; }
        public StatusPlayers Status { get; set; }


        public string UserId { get; set; }
        public User User { get; set; }
        public string TournamentPlayerId { get; set; }
        public TournamentPlayers TournamentPlayer { get; set; }
        public string TournamentId { get; set; }
        public Tournament Tournament { get; set; }
        public string GuestId { get; set; }
        public Guest Guest { get; set; }

    }
}
