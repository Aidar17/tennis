﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tennis.Models
{
    public class Guest
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
    }
}
