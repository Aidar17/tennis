﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Tennis.ViewModels;

namespace Tennis.Models
{
    public class Rating
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public int Points { get; set; }
        public Type Type { get; set; }
        public CategoryPerson Level { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
    }
}
