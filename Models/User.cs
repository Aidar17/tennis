using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Tennis.ViewModels;
namespace Tennis.Models
{
    public class User :IdentityUser
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Points { get; set; }
        public CategoryPerson Category { get; set; }
        public Gender Gender { get; set; }
        public StatusPerson Status { get; set; }
        public string Avatar { get; set; }
        public DateTime ContributionTime { get; set; }
    }
}
