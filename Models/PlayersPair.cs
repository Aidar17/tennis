﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Tennis.Models
{
    public class PlayersPair
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public int Position { get; set; }
        public string TournamentPlayersFirstId { get; set; }
        public TournamentPlayers TournamentPlayersFirst { get; set; }
        public string TournamentPlayersSecondId { get; set; }
        public TournamentPlayers TournamentPlayersSecond { get; set; }
        public string TournamentId { get; set; }
        public Tournament Tournament { get; set; }
    }
}
