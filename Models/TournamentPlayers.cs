﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tennis.Models
{
    public enum StatusPlayers
    {
        In_Tour,
        Not_approve,
        Wait,
        Wait_In_Spare
    }
    public class TournamentPlayers
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public StatusPlayers Status { get; set; }
        public int Position { get; set; }
        public string Preferences { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public string TournamentId { get; set; }
        public Tournament Tournament { get; set; }
        public string GuestId { get; set; }
        public Guest Guest { get; set; }
    }
}
