﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Tennis.Models
{
    public class UsersContext : IdentityDbContext<User>
    {
        public DbSet<User> ContextUsers { get; set; }
        public DbSet<Tournament> Tournaments { get; set; }
        public DbSet<UserTournamentScore> userTournaments { get; set; }
        public DbSet<Sponsor> Sponsors { get; set;}
        public DbSet<News> News { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<TournamentPlayers> AllTournamentPlayers { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<PlayersPair> PlayersPairs { get; set; }

        public UsersContext(DbContextOptions<UsersContext> options)
             : base(options)
        {
        }
    }
}