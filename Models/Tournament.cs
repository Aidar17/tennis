﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Tennis.Models
{
    public enum CategoryTournament
    {
        Futures,
        Masters,
        Promasters
    }

    public enum CategoryLeagueTournament
    {
        Highest,
        First,
        Second,
        Third,
        Fourth
    }

    public enum Type
    {
        ManOne,
        WomenOne,
        ManTwo,
        WomenTwo,
        Mixed
    }

    public enum StatusTournament
    {
        Was,
        Will,
    }

    public enum TournamentScoreStatus
    {
        On,
        Off

    }
    public enum TournamentQueueStatus
    {
        MainStaff,
        Spare
    }

    public enum TournamentRate
    {
        Superior,
        First,
        Second,
        Third,
        Fourth
    }

    public class Tournament
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public CategoryTournament Category { get; set; }
        public CategoryLeagueTournament CategoryLeagueTournament { get; set; }
        public Type Type { get; set; }
        [Required]
        public int UserCount { get; set; }
        public StatusTournament Status { get; set; }

    }
}
