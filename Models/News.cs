﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Tennis.Models
{
    public class News
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string Topic { get; set; }
        public string Content { get; set; }
        public DateTime DateOfPublication { get; set; }
        public string Photo { get; set; }
    }
}
