﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.languages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.util;
using System.Web;

namespace Tennis.Models
{//Этот класс был создан но еще не используется,
 //    предназначен для того чтобы скачать список пользователей в виде PDF документа.
 // Нужно доработать так как не отображается кириллица в PDF документе.

    public class UserReport
    {
        int _totalColumn = 5;
        Document _document;
        Font _fontstyle;
        PdfPTable _pdfTable = new PdfPTable(5);
        PdfPCell _pdfPCell;
        MemoryStream _memoryStream = new MemoryStream();
        List<User> _users = new List<User>();


     
               

        public byte[] PrepareReport(List<User> users)
        {
            _users = users;
            _document = new Document(PageSize.A4, 0f, 0f, 0f, 0f);
            _document.SetPageSize(PageSize.A4);
            _document.SetMargins(20f, 20f, 20f, 20f);
            _pdfTable.WidthPercentage = 100;
            _pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
            _fontstyle = FontFactory.GetFont("Times", 8f, 1);
            PdfWriter.GetInstance(_document, _memoryStream);
            _document.Open();
            _pdfTable.SetWidths(new float[] { 20f, 150f, 100f, 100f, 100f });

            this.ReportHeader();
            this.ReportBody();
            _pdfTable.HeaderRows = 1;
            _document.Add(_pdfTable);
            _document.Close();
            return _memoryStream.ToArray();
        }
        private void ReportHeader()
        {
            _fontstyle = FontFactory.GetFont("Times", 9f, 1);
            _pdfPCell = new PdfPCell(new Phrase("PDF All users", _fontstyle));
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.Border = 0;
            _pdfPCell.BackgroundColor = BaseColor.WHITE;
            _pdfPCell.ExtraParagraphSpace = 0;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();

        }

        private void ReportBody()
        {
            _fontstyle = FontFactory.GetFont("Times", 8f, 1);
            _pdfPCell = new PdfPCell(new Phrase("Id", _fontstyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase("Email", _fontstyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase("Name", _fontstyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);
            
            _pdfPCell = new PdfPCell(new Phrase("Mobile no", _fontstyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);

            /*Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var enc1251 = Encoding.GetEncoding(1251);

            string fontPath = "wwwroot/fonts/times.ttf";*//*


            var enc1252 = CodePagesEncodingProvider.Instance.GetEncoding(1252);

                    BaseFont bf_russian = BaseFont.CreateFont(
            "wwwroot/fonts/times.ttf",
            "CP1251",
            BaseFont.EMBEDDED);

            Font russian = new Font(bf_russian, 12);*/

            

            _pdfPCell = new PdfPCell(new Phrase("Статус", _fontstyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();



            _fontstyle = FontFactory.GetFont("Times", 8f, 0);
            int serialNumber = 1;
            foreach (var user in _users)
            {
                _fontstyle = FontFactory.GetFont("Times", 8f, 1);
                _pdfPCell = new PdfPCell(new Phrase(serialNumber++.ToString(), _fontstyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);

                _fontstyle = FontFactory.GetFont("Times", 8f, 1);
                _pdfPCell = new PdfPCell(new Phrase(user.Email, _fontstyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);


                _fontstyle = FontFactory.GetFont("Times", 8f, 1);
                _pdfPCell = new PdfPCell(new Phrase(user.Name, _fontstyle ));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);

                _fontstyle = FontFactory.GetFont("Times", 8f, 1);
                _pdfPCell = new PdfPCell(new Phrase(user.PhoneNumber, _fontstyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);

                _fontstyle = FontFactory.GetFont("Times", 8f, 1);
                _pdfPCell = new PdfPCell(new Phrase(user.Status.ToString(), _fontstyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);
                _pdfTable.CompleteRow();
            }

        }

    }
}
