﻿# Tennis
Информационный сайт о деятельности КСЛТ (Кыргызстанского сообщества любителей тенниса).На сайте вы можете зарегистрироваться и после одобрения администратора ,стать членом клуба. В дальнейшем пользователь может зарегистрироваться на турнир, просматривать календарь с турнирами и другой функционал. Можно зарегистрироваться в качестве гостя и участвовать в турнире с одобрения администратора.На сайте есть новостной блог, рейтинг игроков в зависимости от категории.

### Built With


* [ASP.NET](https://dotnet.microsoft.com/apps/aspnet/mvc/)
* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)

### Installation



1. Clone the repo
   ```sh
   git clone https://gitlab.com/Aidar17/tennis.git
   ```
2. Connect to docker and change 'UserID' and  'Password' in appsettings.json
   ```sh
    "DefaultConnection": "Server=127.0.0.1;Port=5432;Database=Tennis;UserId= your ID;Password= your password"
   ```


### Usage

 ![File](file.png)

![Login](login.png)

When you log in as admin, it gives you more options like creating tournaments or adding news to the blog.

![Admin Panel](admin_panel.png)

![Create T](create_t.png)

![Add N](add_n.png)


## Acknowledgments


* [Metanit ](https://metanit.com)
* [GitHub Pages](https://pages.github.com)
* [Font Awesome](https://fontawesome.com)