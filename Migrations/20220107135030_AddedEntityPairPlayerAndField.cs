﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tennis.Migrations
{
    public partial class AddedEntityPairPlayerAndField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TournamentId",
                table: "PlayersPairs",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PlayersPairs_TournamentId",
                table: "PlayersPairs",
                column: "TournamentId");

            migrationBuilder.AddForeignKey(
                name: "FK_PlayersPairs_Tournaments_TournamentId",
                table: "PlayersPairs",
                column: "TournamentId",
                principalTable: "Tournaments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayersPairs_Tournaments_TournamentId",
                table: "PlayersPairs");

            migrationBuilder.DropIndex(
                name: "IX_PlayersPairs_TournamentId",
                table: "PlayersPairs");

            migrationBuilder.DropColumn(
                name: "TournamentId",
                table: "PlayersPairs");
        }
    }
}
